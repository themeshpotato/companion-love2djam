xPos = 0
yPos = 0

xPos2 = 3
yPos2 = 3

local usingPlayer1 = true

local leftReady  = true
local rightReady  = true
local upReady  = true
local downReady  = true

local isRed = true

--sound effects
local move = love.audio.newSource("move.wav", "static")
--

function updatePlayer()
end

function getPlayerOneY()
    return xPos
end

function getPlayerTwoX()
    return yPos
end

function getPlayerTwoY()
    return xPos2
end

function getPlayerOneX()
    return yPos2
end

function drawPlayer()
    love.graphics.setLineWidth(3)
    if math.abs(xPos - xPos2) + math.abs(yPos - yPos2) == 5 then
        isRed = true
    else
        isRed = false
    end

    if isRed then
        love.graphics.setColor(255, 0, 0)
    else
        love.graphics.setColor(15, 56, 15)
    end
    love.graphics.line(200 + xPos * 20 + 10, 100 + yPos * 20 + 10, 200 + xPos2 * 20 + 10, 100 + yPos * 20 + 10)
    love.graphics.line(200 + xPos2 * 20 + 10, 100 + yPos * 20 + 10, 200 + xPos2 * 20 + 10, 100 + yPos2 * 20 + 10)
    --draw player 1
    if usingPlayer1 then
        love.graphics.setColor(100, 76, 45)
    else
        love.graphics.setColor(40, 56, 15)
    end
    love.graphics.rectangle("fill", 200 + xPos * 20, 100 + yPos * 20, 16, 16)
    --draw player 2
    if usingPlayer1 then
        love.graphics.setColor(40, 56, 15)
    else
        love.graphics.setColor(100, 76, 45)
    end
    love.graphics.rectangle("fill", 200 + xPos2 * 20, 100 + yPos2 * 20, 16, 16)
end

function keysPressed(key)
    local xBefore1 = xPos
    local yBefore1 = yPos
    local xBefore2 = xPos2
    local yBefore2 = yPos2

    if key == "z" then
        if usingPlayer1 then
            usingPlayer1 = false
        else
            usingPlayer1 = true
        end
    end

    if key == "up" and upReady then
        if usingPlayer1 then
            if yPos ~= 0 then
                yPos = yPos - 1
                upReady = false
            end
        else
            if yPos2 ~= 0 then
                yPos2 = yPos2 - 1
                upReady = false
            end
        end
    elseif key == "down" and downReady then
        if usingPlayer1 then
            if yPos ~= 20 then
                yPos = yPos + 1
                downReady = false
            end
        else
            if yPos2 ~= 20 then
                yPos2 = yPos2 + 1
                downReady = false
            end
        end
    elseif key == "left" and leftReady then
        if usingPlayer1 then
            if xPos ~= 0 then
                xPos = xPos - 1
                leftReady = false
            end
        else
            if xPos2 ~= 0 then
                xPos2 = xPos2 - 1
                leftReady = false
            end
        end
    elseif key == "right" and rightReady then
        if usingPlayer1 then
            if xPos ~= 20 then
                xPos = xPos + 1
                rightReady = false
            end
        else
            if xPos2 ~= 20 then
                xPos2 = xPos2 + 1
                rightReady = false
            end
        end
    end

    if math.abs(xPos - xPos2) + math.abs(yPos - yPos2) == 6 or xPos == xPos2 and yPos == yPos2 then
        xPos = xBefore1
        yPos = yBefore1
        xPos2 = xBefore2
        yPos2 = yBefore2
    else
        move:setVolume(0.3)
        love.audio.play(move)
    end
end

function keysReleased(key)
    if key == "up" then
        upReady = true
    elseif key == "down" then
        downReady = true
    elseif key == "left" then
        leftReady = true
    elseif key == "right" then
        rightReady = true
    end
end